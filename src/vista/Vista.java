package vista;

import java.io.IOException;
import modelo.Alumno;
import util.Util;

/**
 * Fichero: Vista.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 04-nov-2013
 */
public class Vista {

  public Alumno tomaDatos() throws IOException {

    Alumno alumno = new Alumno();
    Util util = new Util();
    String nombre;
    int edad;

    System.out.println("TOMA DE DATOS");

    System.out.print("Nombre: ");
    nombre = util.pedirString();
    alumno.setNombre(nombre);

    System.out.print("Edad: ");
    edad = util.pedirInt();
    alumno.setEdad(edad);

    return alumno;
  }

  public static void muestraDatos(Alumno p) {

    System.out.println("MOSTRANDO DATOS");

    System.out.println(p.getNombre() + " " + p.getEdad());

  }
}
