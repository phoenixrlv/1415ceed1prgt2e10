
package modelo;

/**
 * Práctica Obligatoria Tema 3
 * @author Paco Aldarias Raya
 * @date 18/10/2013
 */

public class Alumno {

  /*
     Clase Alumno
     Proporciona los constructores para crear Personas
     */
  
  private String nombre;
  private int edad;

  public Alumno() {
    nombre = "Paco";
    edad = 25;
  }

  public Alumno(String n, int e) {
    nombre = n;
    edad = e;
  }

  public String getNombre() {
    return nombre;
  }


  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public int getEdad() {
    return edad;
  }

  public void setEdad(int edad) {
    this.edad = edad;
  }


  
}
