
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Fichero: LeeNumero.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-oct-2013
 */

public class LeeNumero {
  
  public static void main(String[] args ) {
  InputStreamReader input = new InputStreamReader(System.in);
  BufferedReader buffer = new BufferedReader(input);
  int numero;
  String linea;

    try {
      System.out.print("Introduce Numero: ");
      linea = buffer.readLine();
      numero = Integer.parseInt(linea);
      System.out.println("Numero leido: "+numero);
    }
    catch ( IOException | NumberFormatException e) {
    }
  }

}
/* EJECUCION:
Introduce Numero: 1
Numero leido: 1
*/
